package execution;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Еленка on 30.05.2014.
 */
public class XSDParser {
    String xsdAbsPath;
    public XSDParser(String xsdAbsPath) {
        this.xsdAbsPath = xsdAbsPath;
    }

    public List<String> getImportedTypesName(){
        DocumentBuilderFactory dbFactory;
        Document docXML;
        List<String> importsList = new ArrayList<String>();
        try {
            dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            docXML = docBuilder.parse(xsdAbsPath);
            docXML.getDocumentElement().normalize();
            NodeList nodeList = docXML.getElementsByTagName("xs:include");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    importsList.add(nodeList.item(i).getAttributes().getNamedItem("schemaLocation").getNodeValue());
                }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return importsList;
    }
}
