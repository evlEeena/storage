package execution;

import db.Storage;
import serialization.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Еленка on 31.05.2014.
 */
public class Executor {
    private Storage storage = new Storage();
   /* public void authUser(List<String> params) {
        String login, password;
        login = params.get(params.indexOf("-u") + 1);
        password = params.get(params.indexOf("-p") + 1);
        DBCollection users = DBConnection.getConnection().getCollection("users");
        DBObject user = new BasicDBObject()
                .append("user", login)
                .append("password", password);
        DBCursor cursor = users.find(user);
        try {
            if(cursor.size() > 0) {
                //надо что-то придумать получше
                //намного лучше
                System.out.println("okay");
            }
        } finally {
            cursor.close();
        }
    }*/

    public void createType(List<String> params) {
        XJCRunner xjcRunner = new XJCRunner(params);
        xjcRunner.running();
        String xsd = "";
        File file = new File(params.get(1));
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(file.getAbsolutePath()));
            String fileLine;
            while ((fileLine = fileReader.readLine()) != null) {
                xsd+=fileLine;
            }
            //fileReader.close();
            storage.insertTypeInfo(params.get(0), xsd);
        } catch (FileNotFoundException e) {
            Logger.getLogger(Executor.class.getSimpleName()).severe(e.getMessage());
        } catch (IOException e) {
            Logger.getLogger(Executor.class.getSimpleName()).severe(e.getMessage());
        }
        //new File(params.get(1)).delete();
    }

    public void infoType(List<String> params) {
        String info = storage.selectTypeInfo(params.get(0));
        StringBuilder stringBuilder = new StringBuilder(info);
        Pattern pattern = Pattern.compile(".*?>(\\s*)");
        Matcher matcher = pattern.matcher(info);

        PrintWriter fileWriter = null;
        try {
            fileWriter = new PrintWriter(new FileWriter(new File(params.get(1)), true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (matcher.find()) {
            fileWriter.println(matcher.group(0));
        }
        fileWriter.flush();
        fileWriter.close();
    }

    public void insertData(List<String> params) {
        Unmarshal unmarshal =  new Unmarshal(params.get(0), params.get(1));
        unmarshal.unmarshalling();
        unmarshal.insertDataToMongo();
    }

    public void selectData(List<String> params) {
        String packageName = params.get(0);
        String[] dateArrays = params.get(1).split("-");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy,HH:mm:ss");
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = dateFormat.parse(dateArrays[0]);
            toDate = dateFormat.parse(dateArrays[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        new Marshal(packageName, fromDate, toDate).marshalling();
    }
}
