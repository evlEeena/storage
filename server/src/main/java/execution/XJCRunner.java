package execution;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Еленка on 30.05.2014.
 */
public class XJCRunner {
    private String type;
    private String xsdFilePath;
    private String episodeFilePath = System.getProperty("user.dir") + "\\episodes\\" + type + ".episode";


    public XJCRunner(List<String> params) {
        type = params.get(0);
        xsdFilePath = params.get(1);
    }

    public void running() {
        File episodeFile = new File(episodeFilePath);
        //проверить корректность работы xsd parserа
        List<String> includeList = new ArrayList<>();
        //List<String> includeList = new XSDParser(getXsdName()).getImportedTypesName();
        List<String> cmdArgs;
        if (includeList.size() > 0)
            cmdArgs = cmdForImportRunning(includeList);
        else
            cmdArgs = cmdForWithoutImportRunning();
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(cmdArgs);
            processBuilder.directory(new File(System.getProperty("user.dir")));
            processBuilder.start();
        } catch(IOException e) {
            Logger.getLogger(XJCRunner.class.getSimpleName()).severe(e.getMessage());
        }
    }

    private List<String> cmdForImportRunning(List<String> episodeList) {
        List<String> cmdArgs = new ArrayList<>();
        cmdArgs.add("xjc");
        cmdArgs.add("-p");
        cmdArgs.add(type);
        cmdArgs.add("-episode");
        cmdArgs.add(episodeFilePath);
        cmdArgs.add(xsdFilePath);
        cmdArgs.add("-extension");
        for(String episode : episodeList)
        {
            cmdArgs.add("-b");
            cmdArgs.add(episode);
        }
        return cmdArgs;
}

    private List<String> cmdForWithoutImportRunning() {
        List<String> cmdArgs = new ArrayList<>();
        cmdArgs.add("xjc");
        cmdArgs.add("-p");
        cmdArgs.add(type);
        cmdArgs.add("-episode");
        cmdArgs.add(episodeFilePath);
        cmdArgs.add(xsdFilePath);
        return cmdArgs;
    }
}
