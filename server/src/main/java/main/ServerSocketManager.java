package main;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Еленка on 09.06.2014.
 */
public class ServerSocketManager {
    private List<Thread> socketList = new ArrayList<>();

    public void put(Thread t) {
        socketList.add(t);
    }

    public List<Thread> getSocketList() {
        return socketList;
    }
}
