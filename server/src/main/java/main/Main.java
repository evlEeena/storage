package main;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import db.DBConnection;
import server.Server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created by Еленка on 31.05.2014.
 */
public class Main {
    public static void main(String[] args) {
        System.setProperty("user.dir", System.getProperty("user.dir") + "\\src\\main\\java\\");
        System.out.println(System.getProperty("user.dir"));
        ServerSocket serverSocket = null;
        ServerSocketManager serverSocketManager = new ServerSocketManager();
        try {
            serverSocket  = new ServerSocket(4444);
        } catch (IOException e) {
            Logger.getLogger(Main.class.getSimpleName()).severe(e.getMessage());
        }
        if (serverSocket != null) {
            while(true) {
                try {
                    Thread thread = new Thread(new Server(serverSocket.accept()));
                    serverSocketManager.put(thread);
                    thread.start();
                } catch(IOException e) {
                    e.printStackTrace();
                    Logger.getLogger(Main.class.getSimpleName()).severe(e.getMessage());
                }
            }
        }
    }

}
