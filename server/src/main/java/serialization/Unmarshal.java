package serialization;

import com.googlecode.mjorm.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import db.DBConnection;
import db.Storage;
import org.reflections.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import reflection.Reflection;

import javax.xml.bind.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;
import java.io.*;
import java.lang.reflect.*;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by e.paramonova on 20.05.14.
 */
public class Unmarshal {

    private String packageName;
    private String xmlPath;
    private List<?> objectsList = new ArrayList<>();
    private Reflection reflection;

    public Unmarshal(String packageName, String xmlPath) {
        this.packageName = packageName;
        this.xmlPath = xmlPath;
        reflection = new Reflection(packageName);
    }

    public void unmarshalling() {
        try {
            JAXBContext context = JAXBContext.newInstance(packageName);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object classObject = unmarshaller.unmarshal(new File(xmlPath));

            for (Method getter : reflection.getGetters(reflection.getRootClassName()))
                try {
                    //getter.getReturnType();
                    objectsList = (List<?>)getter.invoke(classObject, new Object[]{});
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        createClassMjormXml();
    }
    public void createClassMjormXml() {
        Class typeClass = null;
        try {
            typeClass = Class.forName(reflection.getTypeClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            org.w3c.dom.Element rootElement = doc.createElement("descriptors");
            doc.appendChild(rootElement);

            org.w3c.dom.Element desc = doc.createElement("object");
            rootElement.appendChild(desc);
            Attr attr = doc.createAttribute("class");
            attr.setValue(typeClass.getName());
            desc.setAttributeNode(attr);

            for (Field field : typeClass.getDeclaredFields()) {
                org.w3c.dom.Element prop = doc.createElement("property");
                desc.appendChild(prop);
                Attr attrProp = doc.createAttribute("name");
                attrProp.setValue(field.getName());
                prop.setAttributeNode(attrProp);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(System.getProperty("user.dir") +
                    "\\mjorms\\" + typeClass.getSimpleName() + ".mjorm.xml"));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public void insertDataToMongo() {
        XmlDescriptorObjectMapper objectMapper = new XmlDescriptorObjectMapper();
        String folderPath = System.getProperty("user.dir") + "\\mjorms\\";
        File folder = new File(folderPath);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File " + listOfFiles[i].getName());
                try {
                    objectMapper.addXmlObjectDescriptor(new File(folderPath + listOfFiles[i].getName()));
                } catch (XPathExpressionException | IOException | ParserConfigurationException |
                        SAXException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        Class c = null;
        try {
            c = Class.forName(reflection.getTypeClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

       /* DB db = DBConnection.getConnection();
        MongoDao dao = new MongoDaoImpl(db, objectMapper);*/
        Object[] objectArrays = new Object[2];
        Storage storage = new Storage();
        for (Object o : objectsList) {
            objectArrays[0] = c.cast(o);
            objectArrays[1] = new SimpleDateFormat("dd.MM.yyyy,hh:mm:ss").format(new Date());
            storage.insertData(objectMapper, packageName, objectArrays);
            /*dao.createObjects(packageName, objectArrays);
            dao.createObject(packageName, c.cast(o));*/
        }
    }
}
