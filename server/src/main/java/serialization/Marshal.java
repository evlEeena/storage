package serialization;

import com.googlecode.mjorm.*;
import com.mongodb.*;
import db.DBConnection;
import org.xml.sax.SAXException;
import reflection.Reflection;
import streams.ClientOutputStream;

import javax.xml.bind.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.*;
import java.util.*;

/**
 * Created by Еленка on 07.06.2014.
 */
public class Marshal {
    private Class objectClass;
    private String packageName;
    private Date fromDate, toDate;
    private Reflection reflection;

    public Marshal(String packageName, Date fromDate, Date toDate) {
        this.packageName = packageName;
        this.fromDate = fromDate;
        this.toDate = toDate;
        reflection = new Reflection(packageName);
    }

    public void marshalling() {
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance(objectClass);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(selectDataFromMongo(), ClientOutputStream.getStream());
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

    private List selectDataFromMongo() {

        XmlDescriptorObjectMapper objectMapper = new XmlDescriptorObjectMapper();
        String folderPath = System.getProperty("user.dir") +
                "\\src\\main\\java\\mjorms\\";
        File folder = new File(folderPath);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File " + listOfFiles[i].getName());
                try {
                    objectMapper.addXmlObjectDescriptor(new File(folderPath + listOfFiles[i].getName()));
                } catch (XPathExpressionException | IOException | ParserConfigurationException |
                        SAXException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        Class objectClass = null;
        try {
            objectClass = Class.forName(reflection.getTypeClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        DBCollection type = DBConnection.getConnection().getCollection(packageName);
        BasicDBObject query = new BasicDBObject();
        query.put("dateAdded", BasicDBObjectBuilder.start("$gte", fromDate).add("$lte", toDate).get());
        DBCursor cursor = type.find(query);
        ObjectIterator objectIterator = new ObjectIterator(cursor, objectMapper, objectClass);
        return objectIterator.readAll();
    }
}
