package db;

import com.googlecode.mjorm.MongoDao;
import com.googlecode.mjorm.MongoDaoImpl;
import com.googlecode.mjorm.XmlDescriptorObjectMapper;
import com.mongodb.*;

/**
 * Created by Еленка on 06.06.2014.
 */
public class Storage {
    public boolean findType(String type) {
        DBCollection types = DBConnection.getConnection().getCollection("types");
        BasicDBObject query = new BasicDBObject();
        query.put("type", type);
        DBCursor cursor = types.find(query);
        return (cursor.hasNext());
    }

    public boolean findUser(String login, String password) {
        DBCollection users = DBConnection.getConnection().getCollection("users");
        BasicDBObject query = new BasicDBObject();
        query.put("user", login);
        query.put("password", password);
        DBCursor cursor = users.find(query);
        return (cursor.hasNext());
    }

    public void insertTypeInfo(String type, String info) {
        DBCollection types = DBConnection.getConnection().getCollection("types");
        DBObject object = new BasicDBObject()
                .append("type", type)
                .append("xsd", info);
        types.insert(object);
    }
    public String selectTypeInfo(String type) {
        String info = "";
        DBCollection types = DBConnection.getConnection().getCollection("types");
        DBObject object = new BasicDBObject()
                .append("type", type);
        DBCursor cursor = types.find(object);
        if(cursor.hasNext()) {
           info = (String)cursor.next().get("xsd");
        }
        return info;
    }

    public void insertData(XmlDescriptorObjectMapper objectMapper, String type, Object[] objects) {
        DB db = DBConnection.getConnection();
        MongoDao dao = new MongoDaoImpl(db, objectMapper);
        //dao.createObjects(type, objects);
    }
}
