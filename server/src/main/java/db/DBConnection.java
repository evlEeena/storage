
package db;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Еленка on 18.05.2014.
 */

public class DBConnection {
    private static String host, dbname, dbuser, dbpass;
    private static Integer port;

    private static void readConnectionProporties() {
        Properties prop = new Properties();
        FileInputStream in = null;
        try {
            in = new FileInputStream(System.getProperty("user.dir") +
                     "\\db\\database.proporties");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, "Could not open file database.proporties", ex);
            System.exit(-1);
        }
        try {
            prop.load(in);
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE,
                    "Could not load proporties or close file database.proporties", ex);
        }

        host = prop.getProperty("host");
        port = Integer.valueOf(prop.getProperty("port"));
        dbname = prop.getProperty("dbname");
        dbuser = prop.getProperty("dbuser");
        dbpass = prop.getProperty("dbpass");
    }
    public static DB getConnection() {
        readConnectionProporties();
        MongoClient mongoClient = null;
        try {
            mongoClient = new MongoClient(new ServerAddress(host, port));
        } catch (UnknownHostException e) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE,
                    "Unknown host in database.proporties", e);
            e.printStackTrace();
            System.exit(-1);
        }
        DB db = mongoClient.getDB(dbname);
        return  db;
    }
}
