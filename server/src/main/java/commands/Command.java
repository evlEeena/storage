package commands;

import java.util.List;

/**
 * Created by e.paramonova on 19.05.14.
 */
public abstract class Command {
    protected List<String> params;
    public void setParameters(List<String> params) {
        this.params = params;
    }
    public abstract void execute();
}
