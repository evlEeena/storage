/**
 * Created by e.paramonova on 19.05.14.
 */
package commands;

import execution.*;

public class InsertDataCommand extends Command {
    @Override
    public void execute() {
        new Executor().insertData(params);
    }
}
