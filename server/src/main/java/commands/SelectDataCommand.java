/**
 * Created by e.paramonova on 19.05.14.
 */
package commands;


import execution.Executor;

public class SelectDataCommand extends Command {
    @Override
    public void execute() {
        new Executor().selectData(params);
    }
}
