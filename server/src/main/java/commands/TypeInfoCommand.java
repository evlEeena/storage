package commands;

import execution.Executor;

/**
 * Created by e.paramonova on 19.05.14.
 */

public class TypeInfoCommand extends Command{
    @Override
    public void execute() {
        new Executor().infoType(params);
    }
}
