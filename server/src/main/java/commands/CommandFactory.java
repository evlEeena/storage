package commands;

import server.FileNameGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Еленка on 31.05.2014.
 */
public class CommandFactory {
    private String cmdString, action, params;
    private Command command;

    public CommandFactory(String cmdString) {
        this.cmdString = cmdString;
        String[] cmdArray = cmdString.split("/");
        action = cmdArray[0];
        params = cmdArray[1];
    }

    public Command createCommand() {
        switch (action) {
            /*case "auth":
                command = new UserAuthCommand();
                break;*/
            case "create":
                command = new CreateTypeCommand();
                break;
            case "info":
                command = new TypeInfoCommand();
                break;
            case "insert":
                command = new InsertDataCommand();
                break;
            case "select":
                command = new SelectDataCommand();
                break;
        }
        return command;
    }
    public void executeCommand() {
        command.setParameters(createParamsList());
        command.execute();
    }

    private List<String> createParamsList(){
        List<String> paramsList = new ArrayList<>();
        paramsList.add(params);
        paramsList.add(new FileNameGenerator(cmdString).getFileName());
        return  paramsList;
    }
}
