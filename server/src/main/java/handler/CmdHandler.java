package handler;

import db.Storage;

/**
 * Created by Еленка on 31.05.2014.
 */
public class CmdHandler {
    private int errorCode;
    private String[] errorArray = {"Incorrect login or password", "Type does not exists",
            "Type already exists"};
    private String[] commandArray;
    private String action, params;

    public CmdHandler(String cmd) {
        commandArray = cmd.split("/");
        action = commandArray[0];
        params = commandArray[1];
    }

    public boolean checking() {
        Storage storage =  new Storage();
        boolean result = false;
        switch (action) {
            case "auth":
                String[] loginParamsArray = params.split(":");
                result = storage.findUser(loginParamsArray[0], loginParamsArray[1]);
                if (!result) {
                    errorCode = 0;
                }
                break;
            case "create":
                result = !storage.findType(params);
                if (!result) {
                    errorCode = 2;
                }
                break;
            case "info":
            case "insert":
                result = storage.findType(params);
                if (!result) {
                    errorCode = 1;
                }
                break;
            case "select":
                String[] selectParamsArray = params.split("/");
                result = storage.findType(selectParamsArray[0]);
                if (!result) {
                    errorCode = 1;
                }
                break;
        }
        return result;
    }

    public String getErrorMessage() {
        return errorArray[errorCode];
    }

    public String getAction() {
        return action;
    }
}
