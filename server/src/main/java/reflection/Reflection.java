package reflection;

import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.reflections.Store;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.reflections.ReflectionUtils.withModifier;

/**
 * Created by Еленка on 31.05.2014.
 */
public class Reflection {

    private Reflections reflections;
    private Store store;
    private String packageName;
    private List<?> objectsList = new ArrayList<>();

    public Reflection(String packageName) {
        this.packageName = packageName;
        reflections = new Reflections(packageName);
        store = reflections.getStore();
    }

    public String getRootClassName() {
        List<String> rootTypesList = new ArrayList<>
                (store.getTypesAnnotatedWith("javax.xml.bind.annotation.XmlRootElement"));
        //т.к. может быть только 1 root элемент,
        // т.е. rootTypesList.size() всегда == 1, возвращаем 1 элемент списка
        return rootTypesList.get(0);
    }

    public String getTypeClassName() {
        List<String> typesList = new ArrayList<>
                (store.getTypesAnnotatedWith("javax.xml.bind.annotation.XmlType"));
        typesList.remove(getRootClassName());
        //т.к. в соответствии с правилами построения xsd может быть только 1 type элемент,
        // т.е. typesList.size() всегда == 1, возвращаем 1 элемент списка
        return typesList.get(0);

    }

    public Set<Method> getGetters(String className) {
        Set<Method> getters = null;
        Class classObject = null;
        try {
            classObject = Class.forName(className);
            getters = ReflectionUtils.getAllMethods(classObject,
                    withModifier(Modifier.PUBLIC), ReflectionUtils.withPrefix("get"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return getters;
    }
}
