package server;

import java.io.*;

/**
 * Created by Еленка on 31.05.2014.
 */
public class Receiver {
        public void receiveFile(String filePath, BufferedReader in) {
            File file = new File(filePath);
            try {
                PrintWriter fileWriter = new PrintWriter(new FileWriter(file, true));
                String line;
                while (!(line = in.readLine()).equals("#EOF#")) {
                    fileWriter.println(line);
                }
                fileWriter.flush();
                fileWriter.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}
