package server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Еленка on 31.05.2014.
 */
public class Sender {
    public void sendFile(String filePath, PrintWriter out) {
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
            String fileLine;

            while ((fileLine = fileReader.readLine()) != null) {
                out.println(fileLine);
            }
            out.println("#EOF#");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
