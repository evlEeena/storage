package server;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by Еленка on 11.06.2014.
 */
public class FileNameGenerator {
    private String action;
    private String params;
    public FileNameGenerator(String command) {
        String[] paramsArray = command.split("/");
        action = paramsArray[0];
        params = paramsArray[1];
    }
    public  String getFileName() {
        String fileName = System.getProperty("user.dir") + "\\files\\";
        switch (action) {
            case "info":
                fileName += "\\info\\" + params + ".xsd";
                break;
            case "create":
                fileName += "\\create\\" + params + ".xsd";
                break;
            case "insert":
                fileName += "\\insert\\" + params + "_"+ new SimpleDateFormat("dd.MM.yyyy,hh_mm_ss").format(new Date()) +".xml";
                break;
            case "select":
                String[] paramsArray = params.split("/");
                fileName += "\\select\\" + paramsArray[0] + "_" + paramsArray[1] + ".xml";
                break;
        }
        return fileName;
    }
}
