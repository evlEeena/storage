package server;

import commands.CommandFactory;
import handler.CmdHandler;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created by e.paramonova on 22.05.14.
 */
public class Server implements Runnable {

    private Socket client;
    private BufferedReader in;
    private PrintWriter out;

    public Server(Socket client) {
        this.client = client;
        try {
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            out = new PrintWriter(client.getOutputStream(), true);
        } catch (IOException e) {
            //log.error(e);
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String input;
            try {
                while ((input = in.readLine()) != null) {
                    System.out.println(input);
                    //log.info("User send command: " + input);
                    CmdHandler handler = new CmdHandler(input);
                    if (handler.checking()) {
                        out.println("ok");
                        CommandFactory commandFactory = new CommandFactory(input);
                        commandFactory.createCommand();
                        if(handler.getAction().equals("create")|| (handler.getAction().equals("insert"))) {
                            System.out.println(new FileNameGenerator(input).getFileName());
                            File file = new File(new FileNameGenerator(input).getFileName());
                            if(!file.exists()) {
                                file.createNewFile();
                            }
                            new Receiver().receiveFile(file.getAbsolutePath(), in);
                            commandFactory.executeCommand();
                        }
                        commandFactory.executeCommand();
                        if(handler.getAction().equals("info")|| (handler.getAction().equals("select"))) {
                            new Sender().sendFile(new FileNameGenerator(input).getFileName(), out);
                        }
                    }

                    else {
                        out.println(handler.getErrorMessage());
                    }
                }

                closeClient();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }

    private void closeClient() {
        try {
            in.close();
            out.close();
            client.close();
        } catch (IOException e) {
            Logger.getLogger(Server.class.getSimpleName()).severe(e.getMessage());
        }
    }
}
