package client;

import handle.CmdHandler;
import help.Helper;

import java.io.*;
import java.net.*;

import java.util.logging.Logger;

/**
 * Created by Еленка on 31.05.2014.
 */
public class Client {
    public void exec() {
        Socket socket;
        try {
            socket  = new Socket(InetAddress.getLocalHost(), 4444);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader consoleReader = new BufferedReader(new InputStreamReader
                    (System.in));
            PrintWriter consoleWriter = new PrintWriter(System.out);
            String output, input;
            while((output = consoleReader.readLine())!= null) {
                if (output.equals("exit")) {
                    out.close();
                    in.close();
                    socket.close();

                    System.exit(0);
                }

                else if (output.contains("help")) {
                    Helper helper = new Helper(output);
                    if (helper.checking()) {
                        System.out.println(helper.getHelp());
                    }

                    else {
                        System.out.println(helper.getErrorMessage());
                    }
                }

                else {
                    CmdHandler cmdHandler = new CmdHandler(output);
                    if (cmdHandler.checking()) {
                        String str = cmdHandler.getCommandToSendServer();
                        out.println(str);
                        String response = in.readLine();
                        if(response.equals("ok")) {
                            switch (cmdHandler.getAction()) {
                                case "info":
                                    new Receiver().receiveFile(System.getProperty("user.dir") + "\\info.xsd", in);
                                    break;
                                case "create":
                                    new Sender().sendFile(cmdHandler.getFilePath(), out);
                                    break;
                                case "insert":
                                    new Sender().sendFile(cmdHandler.getFilePath(), out);
                                    break;
                                case "select":
                                    new Receiver().receiveFile(System.getProperty("user.dir") + "\\" +
                                            cmdHandler.getType() + "\\select" + cmdHandler.getSelectDate() +".xsd", in);
                                    break;
                            }
                        }
                        else {
                            System.out.println(response);
                        }
                    }

                    else {
                        System.out.println(cmdHandler.getErrorMessage());
                    }
                }
            }
            out.close();
            in.close();
            socket.close();
        } catch (UnknownHostException e) {
            Logger.getLogger(Client.class.getSimpleName()).severe(e.getMessage());
        } catch (IOException e) {
            Logger.getLogger(Client.class.getSimpleName()).severe(e.getMessage());
        }
    }
}
