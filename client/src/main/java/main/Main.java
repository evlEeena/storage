package main;
import client.Client;

/**
 * Created by Еленка on 24.05.2014.
 */
public class Main {
    public static void main(String[] args) {
        System.setProperty("user.dir", System.getProperty("user.dir") + "\\src\\main\\java\\");
        new Client().exec();
    }
}
