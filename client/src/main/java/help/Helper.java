package help;


/**
 * Created by Еленка on 09.06.2014.
 */
public class Helper {
    private int errorCode;
    private String[] cmdArray;
    private String[] actionArray = {"auth", "create", "info", "insert", "select"};
    private String[] errorArray = {"Invalid number of parameters", "Invalid command"};
    public Helper(String cmd) {
        cmdArray = cmd.split(" ");
    }

    public String getHelp() {
            if (cmdArray.length == 1) {
                return HelpPrinter.printHelp();
            }
            else {
                switch (cmdArray[1]) {
                    case "auth":
                        return HelpPrinter.printAuthHelp();
                    case "create":
                        return HelpPrinter.printCreateHelp();
                    case "info":
                        return HelpPrinter.printInfoHelp();
                    case "insert":
                        return HelpPrinter.printInsertHelp();
                    case "select":
                        return HelpPrinter.printSelectHelp();
                }
            }
        return null;
    }


    public boolean checking() {
        if (cmdArray.length > 2) {
            errorCode = 0;
            return false;
        }

        else {
            if (!cmdArray[0].equals("help")) {
                errorCode = 1;
                return false;
            }

            if (cmdArray.length == 2) {
                if (!checkHelpAction()) {
                    errorCode = 1;
                    return false;
                }
            }
        }
        return true;
    }

    public String getErrorMessage() {
        return errorArray[errorCode];
    }

    private boolean checkHelpAction() {
        for (String anActionArray : actionArray) {
            if (cmdArray[1].equals(anActionArray)) {
                return true;
            }
        }
        return false;
    }
}
