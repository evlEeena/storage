package help;

/**
 * Created by Еленка on 19.05.2014.
 */
public class HelpPrinter {

    public static String printHelp() {
        return "Usage: \t\n" +
            "[create|insert|select] [options]\n" +
            "exit  \t exit program\n" +
            "create\t create new type\n" +
            "info  \t get type info\n" +
            "insert\t insert data into storage\n" +
            "select\t select data from storage\n" +
            "options: " + "-h\t\t output help\n";
    }

    public static String printAuthHelp() {
        return "";
    }

    public static String printCreateHelp() {
        return "create\t create new data type in storage\n" +
        "create -t type_name -f file.xsd\n";
    }

    public static String printInfoHelp() {
        return "info  \t get type info\n" +
        "info -t type_name -f file.xml\n";
    }

    public static String printInsertHelp() {
        return "insert\t insert data into storage\n" +
        "insert -t type_name -f file.xsd\n";
    }

    public static String printSelectHelp() {
        return "select\t select data from storage\n" +
        "select -t type_name -f file.xml\n";
    }
}
