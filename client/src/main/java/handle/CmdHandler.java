package handle;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Еленка on 09.06.2014.
 */
public class CmdHandler {
    private String cmd;
    private String prefix, action, params;
    private String commandToServer;
    private int errorCode;
    public CmdHandler(String cmd) {
        this.cmd = cmd;
    }

    private String[] actionArray = {"auth", "create", "info", "insert", "select"};
    private String[] errorArray = {"Invalid command format", "Invalid command prefix",
            "Invalid command action", "Invalid command parameters"};
    private List<String> actionList = new ArrayList<>(Arrays.asList(actionArray));

    public boolean checking() {
        if (!matchCmd()) {
            errorCode = 0;
            return false;
        }

        if(!prefix.equals("storage")) {
            errorCode = 1;
            return false;
        }
        if (!actionList.contains(action)) {
            errorCode = 2;
            return false;
        }

        if (!matchParams()) {
            errorCode = 3;
            return false;
        }

        return true;
    }

    public String getErrorMessage() {
        return errorArray[errorCode];
    }

    public String getCommandToSendServer() {
        return commandToServer;
    }

    private boolean matchCmd() {
        boolean result = false;
        String cmdPattern ="(\\w+)://(\\w+)/(.*)";
        Pattern pattern = Pattern.compile(cmdPattern);
        Matcher matcher = pattern.matcher(cmd);
        while(matcher.find()) {
            result = true;
            prefix = matcher.group(1);
            action = matcher.group(2);
            params = matcher.group(3);
        }
        return result;
    }

    private Matcher matching(String paramsPattern) {
        Pattern pattern = Pattern.compile(paramsPattern);
        return pattern.matcher(params);
    }

    private boolean matchParams() {
        Matcher matcher = null;
        switch(action) {
            case "auth":
                matcher = matching("user=([\\w\\.]+):(\\w+)");
                if (matcher.matches()) {
                    commandToServer = action + "/" + matcher.group(1) + ":" + matcher.group(2);
                }
                break;
            case "info":
                matcher = matching("(\\w+)");
                if (matcher.matches()) {
                    commandToServer = action + "/" + matcher.group(1);
                }
                break;
            case "create":
                matcher = matching("(\\w+)/(path=([\\wа-яА-Я\\\\/:.]+))");
                if (matcher.matches()) {
                    commandToServer = action + "/" + matcher.group(1);
                }
                break;
            case "insert":
                matcher = matching("(\\w+)/(path=([\\wа-яА-Я\\\\/:.]+))");
                if (matcher.matches()) {
                    commandToServer = action + "/" + matcher.group(1);
                }
                break;
            case "select":
                matcher = matching("(\\w+)/(timeperiod=((?:\\d{2}.){2}\\d{4},(?:(?:\\d{2}:){2}\\d{2}))-((?:\\d{2}.){2}\\d{4},(?:(?:\\d{2}:){2}\\d{2})))");
                if (matcher.matches()) {
                    commandToServer = action + "/" + matcher.group(1) + "/" + matcher.group(3) + "-" + matcher.group(4);
                }
                break;
        }
        return matcher.matches();
    }

    public String getAction() {
        return action;
    }

    public String getType() {

        int index = cmd.lastIndexOf("/" + action + "/");
        return cmd.substring(index + 1);
    }

    public String getFilePath() {
        if(action.equals("create") || action.equals("insert")) {
            String str = "path=";
            int index = cmd.lastIndexOf(str);
            return cmd.substring(index + (str.length()));
        }
        return "";
    }

    public String getSelectDate() {
        String str = "timeperiod=";
        int index = cmd.lastIndexOf(str);
        return cmd.substring(index + (str.length()));
    }
}
