# README #

Универсальное хранилище данных для системы управления гетерогенными ip-сетями. Позволяет создавать новые типы данных в хранилище, коим является NoSQL база данных MongoDB, а так же управлять этими данными. В работе использовались технологии: JAXB, XSM/XSD, ORM MJORM и др.